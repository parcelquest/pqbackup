Option Strict Off
Option Explicit On
Friend Class clsFtpSvc

   Private Structure TFILEINFO
      Dim sName As String
      Dim dtDate As Date
      Dim lFileSize As Integer
      Dim eItemType As XceedFtpLib.EXFFolderItemType
      Dim sUserData As String
   End Structure

   Dim WithEvents xFtp As XceedFtpLib.XceedFtp
   Dim m_sCurrentLocalPath As String
   Dim m_sCurrentRemotePath As String
   Dim m_sAddress As String
   Dim m_sPort As String
   Dim m_sUserName As String
   Dim m_sPassword As String
   Dim m_bPassive As Boolean
   Dim m_iListCnt As Short
   Dim m_aFileInfo(256) As TFILEINFO

   Public ReadOnly Property ListCount() As Short
      Get
         ListCount = m_iListCnt
      End Get
   End Property

   Public ReadOnly Property ListItem_Name(ByVal iIdx As Short) As String
      Get
         If iIdx >= 0 And iIdx < m_iListCnt Then
            ListItem_Name = m_aFileInfo(iIdx).sName
         Else
            ListItem_Name = ""
         End If
      End Get
   End Property

   Public ReadOnly Property ListItem_Date(ByVal iIdx As Short) As Date
      Get
         If iIdx >= 0 And iIdx < m_iListCnt Then
            ListItem_Date = m_aFileInfo(iIdx).dtDate
         Else
            ListItem_Date = System.Date.FromOADate(0)
         End If
      End Get
   End Property

   Public ReadOnly Property ListItem_Size(ByVal iIdx As Short) As Integer
      Get
         If iIdx >= 0 And iIdx < m_iListCnt Then
            ListItem_Size = m_aFileInfo(iIdx).lFileSize
         Else
            ListItem_Size = 0
         End If
      End Get
   End Property


   Public Property UserName() As String
      Get
         UserName = m_sUserName
      End Get
      Set(ByVal Value As String)
         m_sUserName = Value
      End Set
   End Property


   Public Property Password() As String
      Get
         Password = m_sPassword
      End Get
      Set(ByVal Value As String)
         m_sPassword = Value
      End Set
   End Property


   Public Property ServerName() As String
      Get
         ServerName = m_sAddress
      End Get
      Set(ByVal Value As String)
         m_sAddress = Value
      End Set
   End Property


   Public Property LocalPath() As String
      Get
         LocalPath = m_sCurrentLocalPath
      End Get
      Set(ByVal Value As String)
         m_sCurrentLocalPath = Value
      End Set
   End Property


   Public Property RemotePath() As String
      Get
         RemotePath = m_sCurrentRemotePath
      End Get
      Set(ByVal Value As String)
         m_sCurrentRemotePath = Value
      End Set
   End Property

   ' ============================================================================
   ' This procedure tells the XceedFtp object to connect to the FTP server. The
   ' property values for the server address, port, username and password should
   ' allready be set before calling this procedure
   ' ============================================================================

   Public Sub ConnectToFtpServer(ByVal ftpMode As Boolean)
      xFtp.ServerAddress = m_sAddress
      xFtp.ServerPort = CShort(m_sPort)
      xFtp.UserName = m_sUserName
      xFtp.Password = m_sPassword
      xFtp.PassiveMode = ftpMode
      m_iListCnt = 0

      ' Connect only if not already connected!
      If xFtp.CurrentState = XceedFtpLib.EXFState.fstNotConnected Then
         On Error GoTo LocalError

         ' Connect to the FTP server
         Call xFtp.Connect()

         ' Get the current remote folder
         On Error GoTo LocalError2

         m_sCurrentRemotePath = GetRemoteFolder
      End If
      Exit Sub

LocalError:
      LogMsg(("Error while connecting to '" & xFtp.ServerAddress & "'. Description: " & Err.Description))
      Exit Sub

LocalError2:
      LogMsg(("Error while obtaining current remote folder after connection. Description: " & Err.Description))
   End Sub

   Public Function ConnectToServer(ByVal ftpMode As Boolean) As Boolean
      On Error GoTo LocalError

      xFtp.ServerAddress = m_sAddress
      xFtp.ServerPort = CShort(m_sPort)
      xFtp.UserName = m_sUserName
      xFtp.Password = m_sPassword
      xFtp.PassiveMode = ftpMode
      m_iListCnt = 0

      ' Connect only if not already connected!
      If xFtp.CurrentState = XceedFtpLib.EXFState.fstNotConnected Then
         Call xFtp.Connect()
         m_sCurrentRemotePath = GetRemoteFolder
      End If

      ConnectToServer = True
      Exit Function

LocalError:
      ConnectToServer = False
   End Function

   ' ============================================================================
   ' This procedure calls the XceedFtp object's disconnect method to disconnect
   ' from the FTP server. Any currently running command will be aborted first.
   ' ============================================================================

   Private Sub Disconnect(Optional ByRef bLog As Boolean = True)

      ' If current state is other than connected or not connected, that means
      ' there is an operation currently running and we should abort it first.

      If (xFtp.CurrentState <> XceedFtpLib.EXFState.fstNotConnected) And (xFtp.CurrentState <> XceedFtpLib.EXFState.fstConnected) Then
         xFtp.Abort = True
         While (xFtp.CurrentState <> XceedFtpLib.EXFState.fstNotConnected) And (xFtp.CurrentState <> XceedFtpLib.EXFState.fstConnected)
            System.Windows.Forms.Application.DoEvents()
         End While
      End If

      ' If we get here, there are no commands running. Disconnect only if connected.

      If xFtp.CurrentState <> XceedFtpLib.EXFState.fstNotConnected Then
         If bLog Then
            Call LogMsg("Disconnecting from " & xFtp.ServerAddress)
         End If
         Call xFtp.Disconnect()
      End If
   End Sub

   ' ============================================================================
   ' This procedure will send one or more of the currently selected files.
   ' For each selected item, this procedure checks to see if it is a folder or
   ' a file. If it is a folder, the entire folder's contents are sent by using
   ' the XceedFtp object's SendMultipleFiles method. If the selected item is
   ' a file, it is sent using the SendFile method.
   ' ============================================================================
   Public Function SendFiles(ByRef srcFilename As String, Optional ByRef dstFilename As String = "") As Short
      On Error GoTo LocalError

      ' Folders are uploaded by using the * wildcard with the SendMultipleFiles
      ' method. We query the user to find out if the library should apply
      ' the * wildcard recursively or not.

      If srcFilename = "" Then
         ' Do not send subfolders
         'LogMsg "Send all file in folder: " & m_sCurrentLocalPath
         Call xFtp.SendMultipleFiles(m_sCurrentLocalPath & "\*", m_sCurrentRemotePath, True, False)
         ' Yes, send subfolders
         'Call xFtp.SendMultipleFiles(m_sCurrentLocalPath & "\*", m_sCurrentRemotePath, True, True)
      Else
         ' Upload this item separately. If file exists, we'll overwrite it.
         'LogMsg "Send file: " & srcFilename
         Call xFtp.SendFile(srcFilename, 0, dstFilename, False)
      End If
      If Err.Number = 0 Then
         Call LogMsg("Upload successfully.")
         SendFiles = 0
      Else
         Call LogMsg("Error while uploading file(s). Description: " & Err.Description)
         SendFiles = 1
      End If

      Exit Function

LocalError:
      SendFiles = 2
      If Err.Number = XceedFtpLib.EXFError.FTP_E_OPERATIONABORTED Then
         Call LogMsg("Upload successfully aborted.")
      Else
         Call LogMsg("Error while ABORT uploading file(s). Description: " & Err.Description)
      End If
   End Function

   ' ============================================================================
   ' This procedure will receive the selected files or folders from the FTP
   ' server. For folders, the procedure uses the XceedFtp object's
   ' ReceiveMultipleFiles method. For files, it uses the ReceiveFile method.
   ' ============================================================================

   Public Function ReceiveFiles(ByRef sRemoteFile As String, ByRef sLocalFile As String, Optional ByRef bLog As Boolean = True) As Short
      On Error GoTo LocalError

      ' Receive the contents of the folder by using the * wildcard and the
      ' ReceiveMultipleFiles method. We query the user to find out if the
      ' library should apply the * wildcard recursively or not.

      If sRemoteFile = "" Then
         ' Receive subfolders
         'Call xFtp.ReceiveMultipleFiles(m_sCurrentRemotePath & "\*", m_sCurrentLocalPath, True)
         Call xFtp.ReceiveMultipleFiles(m_sCurrentRemotePath & "\*", m_sCurrentLocalPath, False)
      Else
         Call xFtp.ReceiveFile(sRemoteFile, 0, sLocalFile)
      End If
      'xFtp.ListFolderContents "*.flg"
      ReceiveFiles = 0

      Exit Function

LocalError:
      ReceiveFiles = 1
      If bLog Then
         Call LogMsg("***** Error while downloading file(s). Description: " & Err.Description)
      End If
   End Function

   Public Function ReceiveFile(ByRef sRemoteFile As String, ByRef sLocalFile As String, Optional ByRef bLog As Boolean = True) As Short
      On Error GoTo LocalError

      'Receive the whole file
      If xFtp.CurrentState = XceedFtpLib.EXFState.fstConnected Then
         Call xFtp.ReceiveFile(sRemoteFile, 0, sLocalFile)
         ReceiveFile = 0
      Else
         LogMsg("***** Error server not connected")
         ReceiveFile = 1
      End If
      Exit Function

LocalError:
      ReceiveFile = 1
      If bLog Then
         Call LogMsg("***** Error while downloading file(s). Description: " & Err.Description)
      End If
   End Function

   ' ============================================================================
   ' This procedure will remove the selected files from the FTP server.
   ' ============================================================================

   Public Function DeleteFile(ByRef sRemoteFile As String) As Short
      On Error GoTo LocalError

      If sRemoteFile <> "" Then
         xFtp.DeleteFile(sRemoteFile)
      End If
      DeleteFile = 0
      Exit Function

LocalError:
      DeleteFile = 1
      Call LogMsg("Error deleting file. Description: " & Err.Description)
   End Function

   ' ============================================================================
   ' This procedure will remove the selected folder from the FTP server.  Some server
   ' may refuse to delete a not-empty folder.
   ' ============================================================================

   Public Function DeleteFolder(ByRef sRemoteFolder As String) As Short
      On Error GoTo LocalError

      If sRemoteFolder <> "" Then
         'UPGRADE_WARNING: Couldn't resolve default property of object xFtp.DeleteFolder. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
         xFtp.DeleteFolder(sRemoteFolder)
      End If
      DeleteFolder = 0
      Exit Function

LocalError:
      DeleteFolder = 1
      Call LogMsg("Error deleting folder. Description: " & Err.Description)
   End Function

   ' ============================================================================
   ' This function returns the current remote folder (the XceedFtp object's
   ' CurrentFolder property is consulted) but also adds the path to the remote
   ' folder listing combo box.
   ' ============================================================================

   Public Function GetRemoteFolder() As String

      On Error GoTo LocalError

      GetRemoteFolder = xFtp.CurrentFolder

      Exit Function

LocalError:
      Call LogMsg("Error while querying current remote folder. Description: " & Err.Description)
      GetRemoteFolder = ""

   End Function

   ' ============================================================================
   ' Change the current remote folder to the new one specifed. Adds new folder
   ' to remote folder list combo box. Returns True if successful. If the
   ' bInteractive parameter is set to False this function will not display any
   ' errors on screen.
   ' ============================================================================

   Public Function ChangeCurrentRemoteFolder(ByVal sNewRemotePath As String, Optional ByRef bLog As Boolean = True) As Boolean

      ChangeCurrentRemoteFolder = False

      On Error GoTo LocalError

      ' Change the current remote folder
      Call xFtp.ChangeCurrentFolder(sNewRemotePath)

      ' Obtain the new current remote folder path from the FTP control
      On Error GoTo LocalError2

      m_sCurrentRemotePath = xFtp.CurrentFolder
      ' We successfully changed folders
      ChangeCurrentRemoteFolder = True
      Exit Function

LocalError:
      If bLog Then
         Call LogMsg("Error while attempting to change current remote folder to '" & sNewRemotePath & "'. Description: " & Err.Description)
      End If
      Exit Function

LocalError2:
      If bLog Then
         Call LogMsg("Error obtaining new current remote folder while attempting to change current remote folder to '" & sNewRemotePath & "'. Description: " & Err.Description)
      End If
   End Function

   ' ============================================================================
   ' Change the current local folder to the new one specifed. Adds new folder to
   ' local folder list combo box. Returns True if successful.
   ' ============================================================================

   Public Function ChangeCurrentLocalFolder(ByVal sNewLocalPath As String, Optional ByRef bLog As Boolean = True) As Boolean

      ChangeCurrentLocalFolder = False

      On Error GoTo LocalError

      ' Change the current local folder
      m_sCurrentLocalPath = sNewLocalPath

      ChangeCurrentLocalFolder = True
      Exit Function

LocalError:

      If bLog Then
         Call LogMsg("Error while attempting to change current local folder to '" & sNewLocalPath & "'. Description: " & Err.Description)
      End If

   End Function

   ' ============================================================================
   ' Abort the current operation (Sets the XceedFtp object's Abort property)
   ' ============================================================================

   Public Sub Abort()
      On Error Resume Next

      xFtp.Abort = True ' Abort the current operation

      If Err.Number <> 0 Then
         LogMsg("Cannot abort the current operation. Error: " & vbCrLf & Err.Description & Err.Number)
         Err.Clear()
      Else
         LogMsg("Abort FTP session")
      End If
   End Sub

   'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Private Sub Class_Initialize_Renamed()
      Dim sCmdLog As String

      'm_sAddress = GetIniData(VB6.GetExeName(), "FTP", "Address")
      'm_sUserName = GetIniData(VB6.GetExeName(), "FTP", "UserName")
      'm_sPassword = GetIniData(VB6.GetExeName(), "FTP", "Password")
      'sCmdLog = GetIniData(VB6.GetExeName(), "FTP", "CommandLog")

      m_sPort = "21"
      m_sCurrentRemotePath = ""
      xFtp = New XceedFtpLib.XceedFtp
      Call xFtp.License("FTP11-CLH2T-CBUAJ-U4BA")
      If sCmdLog <> "" Then
         xFtp.CommandLogFilename = sCmdLog
      End If
   End Sub
   Public Sub New()
      MyBase.New()
      Class_Initialize_Renamed()
   End Sub

   'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
   Private Sub Class_Terminate_Renamed()
      Call Disconnect(False)
   End Sub
   Protected Overrides Sub Finalize()
      Class_Terminate_Renamed()
      MyBase.Finalize()
   End Sub

   Public Sub setBackGround(ByRef bRunASyn As Boolean)
      xFtp.BackgroundProcessing = bRunASyn
   End Sub

   Private Sub xFtp_ListingFolderItem(ByVal eventSender As System.Object, ByVal eventArgs As AxXceedFtpLib.DXceedFtpEvents_ListingFolderItemEvent) Handles xFtp.ListingFolderItem
      If m_iListCnt < 256 Then
         m_aFileInfo(m_iListCnt).eItemType = eItemType
         m_aFileInfo(m_iListCnt).sName = sName
         m_aFileInfo(m_iListCnt).lFileSize = lFileSize
         m_aFileInfo(m_iListCnt).dtDate = dtDate
         m_aFileInfo(m_iListCnt).sUserData = sUserData
         m_iListCnt = m_iListCnt + 1
      End If
   End Sub

   Public Function getFolderListing(ByRef sFileMask As String) As Short
      On Error GoTo LocalError

      ' We will let the Xceed FTP Library parse the FTP server's folder listing for us.
      m_iListCnt = 0
      xFtp.ListParsingFlags = XceedFtpLib.EXFListParsingFlags.flpAutomaticParsing
      Call xFtp.ListFolderContents(sFileMask)

      getFolderListing = m_iListCnt
      Exit Function

LocalError:
      getFolderListing = 0
   End Function
End Class